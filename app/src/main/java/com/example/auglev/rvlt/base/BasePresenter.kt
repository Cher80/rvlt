package com.vk.base

import android.content.Intent
import android.view.KeyEvent

interface BasePresenter {
    fun onDestroy() {}

    fun onStop() {}

    fun onStart() {}

    fun onPause() {}

    fun onResume() {}

    fun onBackPressed() = false

    fun dispatchKeyEvent(event: KeyEvent?) = false

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {}

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {}
}