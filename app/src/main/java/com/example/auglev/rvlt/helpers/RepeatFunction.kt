package com.example.auglev.rvlt.helpers

import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.annotations.NonNull
import io.reactivex.functions.Function
import java.util.concurrent.TimeUnit

class RepeatFunction(var mRepeatDelay: Int, var mRepeatMaxAttempts: Int = -1) : Function<Observable<Any>, ObservableSource<*>> {
    private var mRepeatCurAttempt = 0
    @Throws(Exception::class)
    override fun apply(@NonNull objectObservable: Observable<Any>): ObservableSource<*> {
        return objectObservable.flatMap {
            if (mRepeatCurAttempt++ < mRepeatMaxAttempts || mRepeatMaxAttempts < 0)
                Observable.just(Any()).delay(mRepeatDelay.toLong(), TimeUnit.MILLISECONDS)
            else
                Observable.error<Any>(Throwable())
        }
    }
}
