package com.example.auglev.rvlt

import android.app.Activity
import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class Rvlt : Application(), HasActivityInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>
    var rvltAppComponent: AppComponent? = null

    override fun activityInjector(): AndroidInjector<Activity> = activityInjector

    override fun onCreate() {
        rvltAppComponent = DaggerAppComponent.builder().application(this).build()
        rvltAppComponent?.inject(this)
        super.onCreate()
    }
}