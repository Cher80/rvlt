package com.example.auglev.dagg2.modules

import io.reactivex.Observable

interface Network {

    fun executeRequest(apiProtocol: Protocol = Protocol.HTTPS,
                       apiHost: String = "",
                       endPoint: String = "",
                       params: Map<String, String>? = null,
                       method: Network.Method = Method.GET): Observable<String>

    enum class Method {
        POST, GET
    }

    enum class Protocol(var value: String) {
        HTTPS("https"), HTTP("http")
    }
}