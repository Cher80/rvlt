package com.example.auglev.rvlt.helpers

import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.annotations.NonNull
import io.reactivex.functions.Function
import java.util.concurrent.TimeUnit

class RetryFunction(var mRetryDelay: Int, var mRetryMaxAttempts: Int = -1) : Function<Observable<Throwable>, ObservableSource<*>> {
    private var mRetryCurAttempt = 0

    @Throws(Exception::class)
    override fun apply(@NonNull throwableObservable: Observable<Throwable>): ObservableSource<*> {
        return throwableObservable.flatMap { throwable ->
            if (mRetryCurAttempt++ < mRetryMaxAttempts || mRetryMaxAttempts < 0)
                Observable.just(Any()).delay(mRetryDelay.toLong(), TimeUnit.MILLISECONDS)
            else
                Observable.error<Any>(throwable)
        }
    }
}