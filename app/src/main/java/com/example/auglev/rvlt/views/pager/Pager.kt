package com.example.auglev.rvlt.views.currencies

import com.vk.base.BasePresenter
import com.vk.base.BaseView

class Pager {
    interface View : BaseView {

    }

    interface Presenter : BasePresenter {
        fun start(view:View)
    }
}