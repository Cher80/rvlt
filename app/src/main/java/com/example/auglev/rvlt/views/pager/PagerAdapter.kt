package com.example.auglev.rvlt.views.pager

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.example.auglev.rvlt.views.currencies.CurrenciesView
import com.example.auglev.rvlt.views.rates.RatesView

class PagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
    override fun getItem(position: Int): Fragment = when (position) {
        0 -> RatesView()
        else -> CurrenciesView()
    }

    override fun getCount(): Int = 2

    override fun getPageTitle(position: Int) = when (position) {
        0 -> "All rates"
        else -> "Converter"
    }
}