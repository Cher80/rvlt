package com.example.auglev.rvlt

import com.example.auglev.dagg2.modules.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AndroidSupportInjectionModule::class,
        ActivityBindingModule::class,
        AppModule::class,
        NetworkModule::class,
        SettingsModule::class,
        RatesModule::class,
        APIModule::class,
        PersistModule::class))
interface AppComponent : AndroidInjector<Rvlt> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(dagg2: Rvlt): AppComponent.Builder

        fun build(): AppComponent
    }
}