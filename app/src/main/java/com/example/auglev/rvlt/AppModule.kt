package com.example.auglev.rvlt

import android.app.Application
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjectionModule


@Module(includes = arrayOf(AndroidInjectionModule::class))
abstract class AppModule {
    @Binds
    abstract fun application(mainApplication: Rvlt): Application
}