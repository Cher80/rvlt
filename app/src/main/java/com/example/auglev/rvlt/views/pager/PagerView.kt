package com.example.auglev.rvlt.views.currencies

import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.auglev.rvlt.R
import com.example.auglev.rvlt.views.pager.PagerAdapter
import com.vk.base.BasePresenter
import com.vk.cameraui.widgets.friends.BaseFragment
import javax.inject.Inject


class PagerView : BaseFragment(), Pager.View {

    companion object {
        val TAG = "RVLT_PAGER_VIEW"
    }

    lateinit var pager: ViewPager
    lateinit var tabLayout: TabLayout
    lateinit var appBar: AppBarLayout
    @Inject
    lateinit var presenter: Pager.Presenter

    override fun getPresenter(): BasePresenter {
        return presenter
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        presenter.start(this)
        return inflater?.inflate(R.layout.pager_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pager = view.findViewById(R.id.pager_pager)
        tabLayout = view.findViewById(R.id.pager_tablayout)
        appBar = view.findViewById(R.id.pager_appbar)
        val adapter = PagerAdapter(childFragmentManager)
        pager.adapter = adapter
        tabLayout.setupWithViewPager(pager)
        pager.currentItem = 1

        pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                appBar.setExpanded(true, true)
            }
        })

    }

}

