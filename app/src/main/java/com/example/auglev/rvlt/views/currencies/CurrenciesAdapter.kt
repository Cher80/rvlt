package com.example.auglev.rvlt.views.currencies

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.example.auglev.rvlt.models.ExchangeModel
import com.example.auglev.rvlt.views.currencies.elements.CurrencyView
import java.util.*

class CurrenciesAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var presenter: Currencies.Presenter
    var models: MutableList<ExchangeModel> = LinkedList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = object : RecyclerView.ViewHolder(CurrencyView(parent.context)
            .apply {
                layoutParams = RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT,
                        com.example.auglev.rvlt.helpers.V.toPx(100f, parent.context))
                weakPresenter = java.lang.ref.WeakReference(presenter)
            }
    ) {}

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder.itemView as CurrencyView)?.let {
            it.setModel(presenter.calculateValue(models[position].rate,  presenter.amount), models[position].countryCode)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position)
        } else {
            (holder.itemView as CurrencyView)?.let {
                it.updateValueModel(presenter.calculateValue(models[position].rate,  presenter.amount))
            }
        }
    }

    override fun getItemCount() = models.size
}