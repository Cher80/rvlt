package com.example.auglev.rvlt.views.currencies.elements

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import android.util.Log
import android.view.KeyEvent
import android.widget.EditText
import android.widget.Toast
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.app.Activity
import android.support.v7.widget.AppCompatEditText
import android.view.inputmethod.InputMethodManager


class KeyboardEditText : AppCompatEditText {

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    init {
        setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(this.windowToken, 0)

                hideFocus()
                return@OnEditorActionListener true

            }
            false
        })
    }


    override fun onKeyPreIme(keyCode: Int, event: KeyEvent): Boolean {
        if (event.getKeyCode() === KeyEvent.KEYCODE_BACK && event.getAction() === KeyEvent.ACTION_UP) {
                hideFocus()
        }
        return super.onKeyPreIme(keyCode, event)
    }

    fun hideFocus() {
        isFocusable = false
        isFocusable = true
        isFocusableInTouchMode = true
    }

}