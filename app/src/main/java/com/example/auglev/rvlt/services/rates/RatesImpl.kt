package com.example.auglev.dagg2.modules

import com.example.auglev.rvlt.models.RatesModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers

class RatesImpl(var api: API, var persist: Persist) : Rates {
    override fun getRates(base: String): Observable<RatesModel> {
        return api
                .getRates(base)
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap {
                    persist.saveModel(it)
                    val ratesModel: RatesModel? = persist.getModel(RatesModel::class.java)
                    Observable.just(ratesModel)
                }
    }

    override fun getRatesCached(): RatesModel? {
        val ratesModel: RatesModel? = persist.getModel(RatesModel::class.java)
        return ratesModel
    }

}