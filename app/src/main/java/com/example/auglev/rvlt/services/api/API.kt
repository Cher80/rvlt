package com.example.auglev.dagg2.modules

import com.example.auglev.rvlt.models.RatesModel
import io.reactivex.Observable
import org.json.JSONObject

interface API {
    fun getRates(base: String): Observable<RatesModel>

    companion object {
        fun apiErrorHandler(jsonObject: JSONObject): String? = jsonObject.optString("error")
    }
}