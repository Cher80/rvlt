package com.example.auglev.rvlt.views.currencies

import android.support.v7.util.DiffUtil
import com.example.auglev.rvlt.models.ExchangeModel
import java.util.*


class CurrenciesDiffCallback(var new: List<ExchangeModel>,
                             var old: List<ExchangeModel>,
                             var oldRates: Map<String, Float>,
                             var forceAmountUpdate: Boolean) : DiffUtil.Callback() {

    override fun getOldListSize() = old.size

    override fun getNewListSize() = new.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) = old[oldItemPosition].countryCode == new[newItemPosition].countryCode

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val currencyCode = old[oldItemPosition].countryCode
        return oldRates[currencyCode] == new[newItemPosition].rate && !forceAmountUpdate
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        val l: MutableList<ExchangeModel> = LinkedList()
        l.add(new[newItemPosition])
        return l
    }

}