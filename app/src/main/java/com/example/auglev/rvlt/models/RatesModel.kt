package com.example.auglev.rvlt.models

import com.example.auglev.dagg2.modules.API
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import org.json.JSONObject


open class RatesModel(
        @PrimaryKey var id: String = "TOP",
        var base: String = "",
        var date: String = "",
        var rates: RealmList<ExchangeModel> = RealmList()) : RealmObject() {

    companion object {
        fun fromJsonString(j: String): RatesModel {
            val jsonObject = JSONObject(j)
            val apiError = API.apiErrorHandler(jsonObject)
            if (apiError.isNullOrEmpty()) {
                return RatesModel().apply {
                    base = jsonObject.optString("base")
                    date = jsonObject.optString("date")
                    val jsonRates = jsonObject.optJSONObject("rates")
                    jsonRates?.let {
                        for (k: String in it.keys()) {
                            rates.add(ExchangeModel(k, it.optDouble(k).toFloat()))
                        }
                    }
                }
            } else {
                throw Exception(apiError)
            }
        }
    }
}
