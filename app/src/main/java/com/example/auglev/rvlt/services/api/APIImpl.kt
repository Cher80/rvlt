package com.example.auglev.dagg2.modules

import com.example.auglev.rvlt.models.RatesModel
import io.reactivex.Observable

class APIImpl(var network: Network) : API {

    override fun getRates(base: String): Observable<RatesModel> {
        return network.executeRequest(endPoint = "latest", params = mapOf(Pair("base", base))).flatMap {
            val ratesModel = RatesModel.fromJsonString(it)
            Observable.just(ratesModel)
        }
    }
}