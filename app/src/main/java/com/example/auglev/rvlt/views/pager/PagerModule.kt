package com.example.auglev.rvlt.views.currencies

import com.example.auglev.rvlt.qualifiers.PerFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class PagerModule {

    @PerFragment
    @ContributesAndroidInjector
    abstract fun currenciesView(): PagerView

    @PerFragment
    @Binds
    abstract fun bindPresenter(currenciesPresenter: PagerPresenter): Pager.Presenter
}