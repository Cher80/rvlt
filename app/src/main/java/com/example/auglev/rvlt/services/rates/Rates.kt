package com.example.auglev.dagg2.modules

import com.example.auglev.rvlt.models.RatesModel
import io.reactivex.Observable

interface Rates {
    fun getRates(base: String): Observable<RatesModel>
    fun getRatesCached(): RatesModel?
}