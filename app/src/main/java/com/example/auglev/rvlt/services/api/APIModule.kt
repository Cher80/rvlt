package com.example.auglev.dagg2.modules

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class APIModule {
    @Singleton
    @Provides
    open fun provideAPI(network: Network): API {
        return APIImpl(network)
    }
}