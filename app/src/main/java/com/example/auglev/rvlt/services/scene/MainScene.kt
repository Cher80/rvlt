package com.example.auglev.dagg2.modules

interface MainScene {
    fun showCurrencies()
    fun showPager()
}