package com.example.auglev.dagg2.modules

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SettingsModule {
    @Singleton
    @Provides
    open fun provideSettings(app: Application): Settings {
        return SettingsImpl(app)
    }
}