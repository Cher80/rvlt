package com.example.auglev.dagg2.modules

import io.realm.RealmObject

interface Persist {
    fun <T : RealmObject> saveModel(realmObject: T)
    fun <T : RealmObject> getModel(c: Class<T>): T?
}