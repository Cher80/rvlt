package com.example.auglev.rvlt.views.currencies

import com.example.auglev.rvlt.models.ExchangeModel
import com.example.auglev.rvlt.views.currencies.elements.CurrencyView
import com.vk.base.BasePresenter
import com.vk.base.BaseView

class Currencies {
    interface View : BaseView {
        var adapter: CurrenciesAdapter
        var currencySelected1: CurrencyView
        var currencySelected2: CurrencyView
        fun showSelected(y:Int)
        fun setOnline()
        fun setOffline()
    }

    interface Presenter : BasePresenter {
        var selectedCountryCode:String
        var amount: Float
        fun start(view: View)
        fun refresh()
        fun stopUpdating()
        fun exchangeSelected(currencyCode: String, y: Int)
        fun isSelected(position: Int):Boolean
        fun calculateValue(rate:Float, amount:Float):Float
    }
}