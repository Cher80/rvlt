package com.example.auglev.rvlt

import com.example.auglev.dagg2.modules.MainSceneModule
import com.example.auglev.rvlt.qualifiers.PerActivity
import com.example.auglev.rvlt.views.currencies.CurrenciesModule
import com.example.auglev.rvlt.views.currencies.PagerModule
import dagger.Module
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector

@Module(includes = arrayOf(AndroidInjectionModule::class))
abstract class ActivityBindingModule {
    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(MainSceneModule::class, PagerModule::class, CurrenciesModule::class))
    abstract fun mainActivity(): MainActivity
}