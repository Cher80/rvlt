package com.example.auglev.rvlt.views.currencies.elements

import android.content.Context
import android.graphics.Color
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.auglev.rvlt.R
import com.example.auglev.rvlt.views.currencies.Currencies
import com.squareup.picasso.Picasso
import java.lang.ref.WeakReference


class CurrencyView : LinearLayout {
    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    var weakPresenter: WeakReference<Currencies.Presenter?>? = null

    var value: TextView
    var valueEdit: EditText

    private var explain: TextView
    private var image: ImageView
    private lateinit var currencyCode: String

    init {
        LayoutInflater.from(context).inflate(R.layout.currency_view, this, true)
        value = findViewById(R.id.currency_value)
        explain = findViewById(R.id.currency_explain)
        valueEdit = findViewById(R.id.currency_value_edit)
        image = findViewById(R.id.currency_img)
        setBackgroundColor(Color.parseColor("#FFFFFFFF"))
        orientation = HORIZONTAL

        valueEdit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                try {
                    if (!s?.toString().isNullOrEmpty()) {
                        weakPresenter?.get()?.amount = s.toString().toFloat()
                    } else {
                        weakPresenter?.get()?.amount = 1f
                    }
                } catch (e: NumberFormatException) {

                }

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })

        setOnClickListener {
            val location = IntArray(2)
            getLocationOnScreen(location)
            val y = location[1]
            weakPresenter?.get()?.exchangeSelected(currencyCode, y)
        }
    }

    fun updateValueModel(value: Float) {
        this.value.text = value.toString()
    }

    fun setModel(value: Float, currencyCode: String) {
        this.currencyCode = currencyCode

        //Fix for try.png - resource cant be named "try"
        Picasso.get().cancelRequest(image)
        val imgCurrencyCode = if (currencyCode == "TRY") "tryy" else currencyCode.toLowerCase()
        val resourceId = context.resources.getIdentifier(imgCurrencyCode, "drawable", context.packageName);
        if (resourceId != 0) {
            Picasso.get().load(resourceId).fit().into(image)
        }
        explain.text = context.getString(R.string.currencies_item, currencyCode)
        updateValueModel(value)
    }


}
