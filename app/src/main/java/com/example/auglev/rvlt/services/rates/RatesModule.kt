package com.example.auglev.dagg2.modules

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RatesModule {
    @Singleton
    @Provides
    open fun provideRates(api: API, persist: Persist): Rates {
        return RatesImpl(api, persist)
    }
}