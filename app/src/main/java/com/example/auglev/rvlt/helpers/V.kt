package com.example.auglev.rvlt.helpers

import android.content.Context

class V {
    companion object {
        fun toPx(dp:Float, c:Context?) : Int{
            val scale = c?.resources?.displayMetrics?.density ?: 1f
            val px = (dp * scale + 0.5f)
            return px.toInt()
        }
    }
}