package com.example.auglev.rvlt

import android.os.Bundle
import com.example.auglev.dagg2.modules.MainScene
import com.example.auglev.rvlt.base.RvltBaseActivity
import javax.inject.Inject

class MainActivity : RvltBaseActivity() {
    @Inject
    lateinit var scene: MainScene

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        scene.showPager()
    }

    fun getContainerLayoutId() = R.id.fragments_holder
}
