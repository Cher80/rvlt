package com.example.auglev.rvlt.views.currencies

import android.graphics.Color
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.auglev.rvlt.R
import com.example.auglev.rvlt.views.currencies.elements.CurrencyView
import com.vk.base.BasePresenter
import com.vk.cameraui.widgets.friends.BaseFragment
import java.lang.ref.WeakReference
import javax.inject.Inject

class CurrenciesView : BaseFragment(), Currencies.View {
    companion object {
        val TAG = "RVLT_CURRENCIES_VIEW"
    }

    @Inject
    lateinit var presenter: Currencies.Presenter

    lateinit var online: View
    lateinit var recycler: RecyclerView
    lateinit var layoutManager: LinearLayoutManager
    override var adapter = CurrenciesAdapter()
    override lateinit var currencySelected1: CurrencyView
    override lateinit var currencySelected2: CurrencyView
    lateinit var refresh: SwipeRefreshLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        adapter.presenter = presenter
        return inflater?.inflate(R.layout.currencies, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler = view.findViewById(R.id.currencies_recycler)
        currencySelected1 = view.findViewById(R.id.currencies_selected1)
        currencySelected2 = view.findViewById(R.id.currencies_selected2)
        online = view.findViewById(R.id.currencies_online)
        refresh = view.findViewById(R.id.currencies_swiperefresh)
        currencySelected2.weakPresenter = WeakReference(presenter)

        currencySelected1.value.visibility = View.GONE
        currencySelected2.value.visibility = View.GONE
        currencySelected2.valueEdit.visibility = View.VISIBLE

        recycler.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL).apply {
            this.setDrawable(android.support.v4.content.ContextCompat.getDrawable(context!!, R.drawable.divider)!!)
        })

        layoutManager = LinearLayoutManager(context)
        recycler.adapter = adapter
        recycler.layoutManager = layoutManager

        recycler.itemAnimator = null

        presenter.start(this)

        refresh.setOnRefreshListener {
            refresh.isRefreshing = false
            presenter?.refresh()
        }
    }

    override fun getPresenter(): BasePresenter {
        return presenter
    }

    override fun showSelected(y: Int) {
        val location = IntArray(2)
        currencySelected2.getLocationOnScreen(location)
        val yS = location[1]
        currencySelected2.translationY = (y - yS).toFloat()
        currencySelected2.animate().translationY(0f).setStartDelay(0).setDuration(350).start()
    }

    override fun setOnline() {
        online.setBackgroundColor(Color.GREEN)
    }

    override fun setOffline() {
        online.setBackgroundColor(Color.RED)
    }

}

