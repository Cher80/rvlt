package com.example.auglev.dagg2.modules

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PersistModule {
    @Singleton
    @Provides
    open fun providePersis(application: Application): Persist {
        return PersistImpl(application)
    }
}