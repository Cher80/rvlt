package com.example.auglev.dagg2.modules

import android.app.Application
import com.example.auglev.rvlt.MainActivity
import com.example.auglev.rvlt.qualifiers.PerActivity
import dagger.Module
import dagger.Provides

@Module
class MainSceneModule {
    @PerActivity
    @Provides
    open fun provideMainScene(app: Application, a: MainActivity): MainScene {
        return MainSceneImpl(app, a, a.getContainerLayoutId())
    }
}