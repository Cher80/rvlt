package com.example.auglev.rvlt.views.currencies

import com.example.auglev.rvlt.qualifiers.PerFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class CurrenciesModule {
    @PerFragment
    @ContributesAndroidInjector
    abstract fun currenciesView(): CurrenciesView

    @PerFragment
    @Binds
    abstract fun bindPresenter(currenciesPresenter: CurrenciesPresenter): Currencies.Presenter
}