package com.example.auglev.dagg2.modules

import android.app.Application
import io.realm.Realm
import io.realm.RealmObject


class PersistImpl(var application: Application) : Persist {

    var realm: Realm

    init {
        Realm.init(application)
        realm = Realm.getDefaultInstance()
    }

    override fun <T : RealmObject> saveModel(realmObject: T) {
        realm.beginTransaction()
        realm.copyToRealmOrUpdate(realmObject)
        realm.commitTransaction()
    }

    override fun <T : RealmObject> getModel(c: Class<T>): T? {
        return realm.where(c).findFirst()
    }
}