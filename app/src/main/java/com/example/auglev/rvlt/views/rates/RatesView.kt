package com.example.auglev.rvlt.views.rates

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.auglev.rvlt.R


class RatesView : Fragment() {

    companion object {
        val TAG = "RVLT_RATES_VIEW"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.rates_fragment, container, false)
    }


}

