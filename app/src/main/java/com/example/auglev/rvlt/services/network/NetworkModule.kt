package com.example.auglev.dagg2.modules

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkModule {

    @Singleton
    @Provides
    open fun provideNetwork(app: Application, settings: Settings): Network {
        return NetworkImpl(app, settings)
    }
}