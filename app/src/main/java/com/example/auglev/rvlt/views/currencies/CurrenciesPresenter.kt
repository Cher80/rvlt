package com.example.auglev.rvlt.views.currencies

import android.content.Context
import android.os.SystemClock
import android.support.v7.util.DiffUtil
import android.view.MotionEvent
import com.example.auglev.dagg2.modules.Rates
import com.example.auglev.rvlt.helpers.RepeatFunction
import com.example.auglev.rvlt.helpers.RetryFunction
import com.example.auglev.rvlt.models.ExchangeModel
import com.example.auglev.rvlt.models.RatesModel
import io.reactivex.disposables.Disposable
import java.util.*
import javax.inject.Inject
import kotlin.collections.HashMap
import android.view.inputmethod.InputMethodManager.HIDE_IMPLICIT_ONLY
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.inputmethod.InputMethodManager


class CurrenciesPresenter : Currencies.Presenter {
    companion object {
        const val SWITCH_DELAY = 500
        const val INITIAL_CURRENCY = "RUB"
    }

    @Inject
    lateinit var rates: Rates

    lateinit var view: Currencies.View

    override var amount: Float = 1f
        set(value) {
            field = value
            val diffResult = DiffUtil.calculateDiff(CurrenciesDiffCallback(exchangeModeslCurrent, exchangeModeslCurrent, oldRates, true));
            diffResult.dispatchUpdatesTo(view.adapter)
        }

    private var switchLock = System.currentTimeMillis()
    private var wasPaused: Boolean = false
    private var ratesModel: RatesModel? = null
    private var exchangeModeslCurrent: MutableList<ExchangeModel> = LinkedList()
    private var oldRates: MutableMap<String, Float> = HashMap()
    private var getRatesDisposable: Disposable? = null

    override var selectedCountryCode: String = INITIAL_CURRENCY


    @Inject
    constructor()

    override fun start(view: Currencies.View) {
        this.view = view
        /*
        Load saved state firstly, then refresh
         */
        ratesModel = rates.getRatesCached()
        ratesModel?.let {
            selectedCountryCode = it.base
        }
        populate()
        refresh()
        initSelected()
    }


    private fun initSelected() {
        view.currencySelected1.setModel(1f, selectedCountryCode)
        view.currencySelected2.setModel(1f, selectedCountryCode)
    }

    private fun populate() {
        ratesModel?.let {
            exchangeModeslCurrent.clear()
            exchangeModeslCurrent.addAll(view.adapter.models)

            view.adapter.models.clear()
            view.adapter.models.addAll(it.rates)

            val diffResult = DiffUtil.calculateDiff(CurrenciesDiffCallback(it.rates, exchangeModeslCurrent, oldRates, false));
            diffResult.dispatchUpdatesTo(view.adapter)
        }
    }

    override fun refresh() {
        stopUpdating()
        getRatesDisposable = rates.getRates(selectedCountryCode)
                .doOnError {
                    view.setOffline()
                }
                .repeatWhen(RepeatFunction(1000, -1))
                .retryWhen(RetryFunction(3000, -1))
                .subscribe(
                        {
                            view.setOnline()
                            ratesModel = it
                            populate()
                        })
    }

    override fun exchangeSelected(currencyCode: String, y: Int) {
        if (selectedCountryCode == currencyCode) return
        if (System.currentTimeMillis() - switchLock < SWITCH_DELAY) return

        switchLock = System.currentTimeMillis()
        stopUpdating()

        view.currencySelected1.setModel(1f, selectedCountryCode)

        selectedCountryCode = currencyCode

        view.currencySelected2.setModel(1f, selectedCountryCode)

        view.currencySelected2.requestFocus()
        val imm = view.currencySelected2.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)

        view.showSelected(y)

        refresh()
    }

    override fun calculateValue(rate: Float, amount: Float) = rate * amount

    override fun isSelected(position: Int) = selectedCountryCode == view.adapter.models[position].countryCode

    override fun onPause() {
        wasPaused = true
        stopUpdating()
    }

    override fun onResume() {
        wasPaused = false
        refresh()
    }

    override fun stopUpdating() {
        getRatesDisposable?.dispose()
    }


}