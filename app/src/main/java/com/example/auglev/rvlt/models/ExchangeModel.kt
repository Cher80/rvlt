package com.example.auglev.rvlt.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class ExchangeModel(
        @PrimaryKey var countryCode: String = "",
        var rate: Float = 0f)
    : RealmObject()


