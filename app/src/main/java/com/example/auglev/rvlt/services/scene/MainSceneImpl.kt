package com.example.auglev.dagg2.modules

import android.app.Application
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.example.auglev.rvlt.R
import com.example.auglev.rvlt.views.currencies.CurrenciesView
import com.example.auglev.rvlt.views.currencies.PagerView


class MainSceneImpl(var app: Application, var activity: AppCompatActivity, var containerId: Int) : MainScene {
    override fun showPager() {
        val fm = activity.supportFragmentManager
        val ft = fm.beginTransaction()
        val f = PagerView()
        ft.add(containerId, f)
        ft.commit()
    }

    override fun showCurrencies() {
        val fm = activity.supportFragmentManager
        val ft = fm.beginTransaction()
        val f = CurrenciesView()
        ft.add(R.id.fragments_holder, f)
        ft.commit()
    }

    companion object {
        fun makeTag(f: Fragment, tag: String) = "${tag}_${f.hashCode()}"
    }
}