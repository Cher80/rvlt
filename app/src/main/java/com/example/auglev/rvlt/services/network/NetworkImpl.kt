package com.example.auglev.dagg2.modules

import android.app.Application
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.Call
import okhttp3.OkHttpClient
import okhttp3.Request

class NetworkImpl(var app: Application, var settings: Settings) : Network {

    var ok = OkHttpClient()

    override fun executeRequest(apiProtocol: Network.Protocol,
                                apiHost: String,
                                endPoint: String,
                                params: Map<String, String>?,
                                method: Network.Method) =
            Observable
                    .create(object : ObservableOnSubscribe<String>, Disposable {
                        var disposed = false
                        var c: Call? = null
                        override fun isDisposed() = disposed

                        override fun dispose() {
                            c?.cancel()
                            disposed = true
                        }

                        override fun subscribe(emitter: ObservableEmitter<String>) {
                            emitter.setDisposable(this)
                            val sb = StringBuilder()
                            sb.append(apiProtocol.value)
                            sb.append("://")
                            sb.append(settings.apiHost)
                            sb.append("/")
                            sb.append(endPoint)
                            if (params?.isNotEmpty() == true) {
                                sb.append("?")
                                sb.append(mapToParams(params))
                            }
                            val url = sb.toString()
                            val request = Request.Builder()
                                    .url(url)
                                    .build()
                            try {
                                if (!emitter.isDisposed) {
                                    ok.newCall(request)
                                    c = ok.newCall(request)
                                    val res = c?.execute()?.body()?.string()!!
                                    emitter.onNext(res)
                                    emitter.onComplete()
                                }
                            } catch (e: Exception) {
                                if (!emitter.isDisposed) {
                                    emitter.onError(e)
                                }
                            }
                        }

                    })
                    .subscribeOn(Schedulers.io())!!


    companion object {
        fun mapToParams(map: Map<String, String>): String {
            val sb = StringBuilder()
            var first = true
            map.forEach({
                if (first) {
                    first = false
                } else {
                    sb.append("&")
                }
                sb.append("${it.key}=${it.value}")
            })
            return sb.toString()
        }
    }
}